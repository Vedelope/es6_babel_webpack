// webpack.config.js
var path = require('path');
module.exports = {
	  // Set up an ES6-ish environment
  entry: ['babel-polyfill', './main.js'],
	output: {
		path: __dirname,
		filename: 'bundle.js'
	},
  module: {
	  loaders: [
				{
					test: /.js$/,
					loader: 'babel-loader'
				}
	  ]
  }
};

class Main {
    constructor() {
        console.log('object created');
    }

    update(text) {
        console.log('update: ' + text);
    }
}

var a = new Main();
a.update('codey');
